# from channels.generic.websocket import AsyncWebsocketConsumer
import json
# from channels.consumer import AsyncConsumer
import asyncio
from channels.generic.websocket import AsyncJsonWebsocketConsumer
from channels.layers import get_channel_layer


class UserConsumer(AsyncJsonWebsocketConsumer):
    async def connect(self):
        self.username = "Anonymous"
        await self.accept()
        await self.send_json("User Connected : " + self.username)


    async def disconnect(self, code):
        print("User Disconnected : ", code)

       
    async def receive(self, text_data):
        # data = json.loads(text_data)
        print(text_data)
        await self.send_json(
            {
            "msg_type": "User",
            "room": "room_id",
            "username": "username",
            "message": "message",
            },
        )
        


class DriverConsumer(AsyncJsonWebsocketConsumer):
    async def connect(self):
        await self.accept()
        await self.send_json("Driver Connected")


    async def disconnect(self, code):
        print("Driver Disconnected : ", code)

       
    async def receive(self, text_data):
        print(text_data)
        await self.send_json("Got Response From Driver : " + str(text_data))



class TickTockConsumer(AsyncJsonWebsocketConsumer):

    async def connect(self):
        await self.accept()
        while 1:
            await asyncio.sleep(3)
            await self.send_json("You")
            await asyncio.sleep(3)
            await self.send_json("....Taxi")
