
from channels.routing import ProtocolTypeRouter, URLRouter
from django.urls import path
from YouTaxiWebsite.consumers import UserConsumer, DriverConsumer, TickTockConsumer


application = ProtocolTypeRouter({
    "websocket": URLRouter([
        path("ws/", TickTockConsumer),
        path("ws/user/<slug:slug>", UserConsumer),
        path("ws/driver/", DriverConsumer),
    ])
})