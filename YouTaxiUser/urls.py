from django.urls import path
from .import views

urlpatterns = [
    path('demo/', views.DemoUser, name=""),
    path('create/', views.Create, name=""),
    path('update/', views.Update, name=""),
    path('validate/otp/', views.ValidateOTP, name=""),
    path('request/', views.UserRequest, name=""),
]